package fifo;

public interface IFila {
	void inserir(int valor);
	No remover();
	void imprimir();
}
