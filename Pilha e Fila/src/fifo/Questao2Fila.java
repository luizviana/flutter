package fifo;

import lifo.Pilha;

public class Questao2Fila {

	public static void main(String[] args) {
		Fila fila = new Fila();

		fila.inserir(1);
		fila.inserir(2);
		fila.inserir(3);
		fila.inserir(4);
		fila.inserir(5);
		fila.inserir(6);
		fila.inserir(7);
		System.out.println("Fila:");
		fila.imprimir();

		fila = removerInverterCincoElementos(fila);
		System.out.println("Fila Invertida:");
		fila.imprimir();
	}

	private static Fila removerInverterCincoElementos(Fila fila) {
		Pilha pilha = new Pilha();
		for (int i = 0; i < 5; i++) {
			pilha.push(fila.remover().valor);
		}
		for (int i = 0; i < 5; i++) {
			fila.inserir(pilha.pop().valor);
		}
		return fila;
	}

}
