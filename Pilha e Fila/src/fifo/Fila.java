package fifo;

public class Fila implements IFila {
	
	No inicio = null;

	@Override
	public void inserir(int valor) {
		No novo = new No();
		novo.valor = valor;
		if(inicio == null) {
			inicio = novo;
			return;
		}
		No aux = inicio;
		while(aux.prox!= null) {
			aux = aux.prox;
		}
		
		aux.prox = novo;
		
	}

	@Override
	public No remover() {
		No aux = inicio;
		if(aux != null) {
			inicio = aux.prox;
		}
		return aux;
	}

	
	
	
	@Override
	public void imprimir() {
		No aux = inicio;
		if(inicio ==null) {
			System.out.println("Fila vazia!");
			return;
		}
		do {
			System.out.println(aux.valor);
			aux = aux.prox;
		}while(aux!= null);
	}

}
