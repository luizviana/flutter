package fifo;

public class Questao1Fila {
	public static void main(String[] args) {
		Fila fila = new Fila();
		fila.inserir(1);
		fila.inserir(2);
		fila.inserir(3);
		fila.inserir(4);
		fila.inserir(4);
		fila.inserir(4);
		fila.inserir(4);
		
		int tamanhoFila = medirFila(fila);
		System.out.println("Fila de " + tamanhoFila + " elementos");
	}

	private static int medirFila(Fila fila) {
		int tamanhoFila = 0;
		do {
			fila.remover();
			tamanhoFila++;
		}while(fila.inicio != null);
		return tamanhoFila;
	}
}
