package fifo;

public class TesteFila {

	public static void main(String[] args) {
		executa(new Fila());
	}

	private static void executa(Fila fila) {
		fila.inserir(1);
		fila.inserir(2);
		fila.inserir(3);
		fila.inserir(4);
		fila.imprimir();
		
		System.out.println("\n\n");
		System.out.println("Removido: " + fila.remover().valor);
		fila.imprimir();
		
		System.out.println("\n\n");
		System.out.println("Removido: " + fila.remover().valor);
		fila.imprimir();
		
		System.out.println("\n\n");
		System.out.println("Removido: " + fila.remover().valor);
		fila.imprimir();
		
		System.out.println("\n\n");
		System.out.println("Removido: " + fila.remover().valor);
		fila.imprimir();
	}

}
