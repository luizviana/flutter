package lifo;

public interface IPilha {
	void push(int valor);
	No pop();
	void imprimir();
}
