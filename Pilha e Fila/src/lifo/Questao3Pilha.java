package lifo;

public class Questao3Pilha {
	public static void main(String[] args) {
		Pilha p1 = new Pilha();
		Pilha p2 = new Pilha();
		
		p1.push(1);
		p1.push(2);
		p1.push(3);
		p1.push(4);
		p1.push(5);
		
		p2.push(1);
		p2.push(2);
		p2.push(3);
		p2.push(4);
		p2.push(5);
		
		compararPilhas(p1,p2);
		
	}

	private static void compararPilhas(Pilha p1, Pilha p2) {
		do {
			if(p1.pop().valor != p2.pop().valor) {
				System.out.println("Pilhas diferentes");
				return;
			}
		}while(p1.topo!= null && p2.topo != null);
		
		System.out.println("Pilhas iguais");
	}
}
