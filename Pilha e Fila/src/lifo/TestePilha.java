package lifo;

public class TestePilha {
	public static void main(String[] args) {
		executa(new Pilha());
	}

	private static void executa(Pilha pilha) {
		pilha.push(1);
		pilha.push(2);
		pilha.push(3);
		pilha.push(4);
		pilha.imprimir();
		
		System.out.println("\n\n");
		System.out.println("Removido: " + pilha.pop().valor);
		pilha.imprimir();
		
		System.out.println("\n\n");
		System.out.println("Removido: " + pilha.pop().valor);
		pilha.imprimir();
		
		System.out.println("\n\n");
		System.out.println("Removido: " + pilha.pop().valor);
		pilha.imprimir();
		
		System.out.println("\n\n");
		System.out.println("Removido: " + pilha.pop().valor);
		pilha.imprimir();
		
	}
}
