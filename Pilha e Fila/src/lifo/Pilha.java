package lifo;

public class Pilha implements IPilha {

	 No topo = null;
	 
	@Override
	public void push(int valor) {
		No novo = new No();
		novo.valor = valor;
		novo.prox = topo;
		topo = novo;
		
	}

	@Override
	public No pop() {
		No aux = topo;
		topo = aux.prox;
		return aux;
	}

	@Override
	public void imprimir() {
		if(topo == null) {
			System.out.println("Pilha vazia!");
			return;
		}
		No aux = topo;
		do {
			System.out.println(aux.valor);
			aux = aux.prox;
		}while(aux != null);

	}

}
