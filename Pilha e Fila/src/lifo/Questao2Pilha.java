package lifo;

public class Questao2Pilha {

	public static void main(String[] args) {
		Pilha p = new Pilha();
		p.push(1);
		p.push(2);
		p.push(3);
		p.push(4);
		p.push(5);
		System.out.println("Pilha");
		p.imprimir();
		
		p = inverterPilha(p);
		System.out.println("\nPilha Invertida");
		p.imprimir();
	}

	private static Pilha inverterPilha(Pilha p) {
		Pilha pInvertida = new Pilha();
		do {
			pInvertida.push(p.pop().valor);
		}while(p.topo != null);
		return pInvertida;
	}

}
