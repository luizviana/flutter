package lifo;

public class Questao1Pilha {
	public static void main(String[] args) {
		Pilha p1 = new Pilha();
		p1.push(1);
		p1.push(1);
		p1.push(1);
		p1.push(1);
		Pilha p2 = new Pilha();
		p2.push(1);
		p2.push(1);
		p2.push(1);
		p2.push(1);
		p2.push(1);

		compararTamanho(p1, p2);
	}

	private static void compararTamanho(Pilha p1, Pilha p2) {
		int tamanhop1 = 0;
		do {
			p1.pop();
			tamanhop1++;
		} while (p1.topo != null);

		int tamanhop2 = 0;
		do {
			p2.pop();
			tamanhop2++;
		} while (p2.topo != null);

		if (tamanhop1 > tamanhop2) {
			System.out.println(
					"Tamanho pilha1: " + tamanhop1 + "\nTamanho pilha2: " + tamanhop2 + "\nPilha 1 maior que pilha 2");
		} else if (tamanhop1 < tamanhop2) {
			System.out.println(
					"Tamanho pilha1: " + tamanhop1 + "\nTamanho pilha2: " + tamanhop2 + "\nPilha 2 maior que pilha 1");
		} else
			System.out.println(
					"Tamanho pilha1: " + tamanhop1 + "\nTamanho pilha2: " + tamanhop2 + "\nPilhas de tamanho igual");
	}

}
