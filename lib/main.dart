import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:simple_code/simple_code.dart';

void main() => runApp(new Home());

class Start extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: Home());
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    setSc(context);
    return Scaffold(
        body: Center(
          child: new Column(
            children: <Widget>[
              logo(), 
              nome1(), 
              nome2(), 
              button(), 
              configGeral()
              ],
            ),
          )
        );
      }
}

Row configGeral() {
  return new Row(
    children: <Widget>[config(), config2()],
  );
}

InkWell button() {
  return InkWell(
    onTap: () {
      navigator(context, page: SegundaTela());
    },
    child: new Container(
      margin: EdgeInsets.only(bottom: 5),
      height: 80,
      width: 260,
      decoration: BoxDecoration(
          color: Color(0xFFEE2B2B), borderRadius: BorderRadius.circular(15)),
      child: Center(
          child: Text(
        "Jogar",
        style: TextStyle(
            fontSize: 40,
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: 'TradeGothic'),
      )),
    ),
  );
}

Container config2() {
  return new Container(
    margin: EdgeInsets.only(left: 10, top: 5),
    width: 160,
    height: 28,
    child: Text("Configurações",
        style: TextStyle(
            fontSize: 25, color: Colors.black, fontFamily: 'TradeGothic')),
  );
}

Container config() {
  return new Container(
    margin: EdgeInsets.only(left: 110, top: 1),
    width: 30,
    height: 30,
    child: Image(
      image: AssetImage('lib/assets/images/settings.png'),
    ),
  );
}

Container nome2() {
  return new Container(
    margin: EdgeInsets.only(bottom: 100),
    height: 60,
    width: 190,
    child: Center(
        child: Text("ou Trepa",
            style: TextStyle(
                color: Color(0xFFEE2B2B),
                fontSize: 50,
                fontFamily: 'TradeGothic',
                fontWeight: FontWeight.bold))),
  );
}

Container nome1() {
  return new Container(
    margin: EdgeInsets.only(top: 20),
    height: 60,
    width: 240,
    child: Center(
        child: Text("Casa, Mata",
            style: TextStyle(
                color: Color(0xFFEE2B2B),
                fontSize: 50,
                fontFamily: 'TradeGothic',
                fontWeight: FontWeight.bold))),
  );
}

Container logo() {
  return new Container(
    margin: EdgeInsets.only(top: 50),
    child: Image(
      image: AssetImage('lib/assets/images/logo.png'),
    ),
    height: 250,
    width: 250,
  );
}

class SegundaTela extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    setSc(context);
    return Scaffold(
        backgroundColor: Colors.white,
        body: new Container(
          width: 300,
          height: 300,
          color: Colors.black,
        ));
  }
}
